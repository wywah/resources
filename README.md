# Resources

> 个人搜集学习资源  
>
> <kbd>始于20201208</kbd>

## 公共资源

### 开源项目

#### Java

- [**若依** ](https://gitee.com/y_project/RuoYi)

  > 基于 SpringBoot

- [**knife4j**](https://gitee.com/xiaoym/knife4j)

  > knife4j是为Java MVC框架集成Swagger生成Api文档的工具  
  >
  > gitee : https://gitee.com/xiaoym/knife4j

- ## [人人开源](https://gitee.com/renrenio)

  > 人人开源

- [**mybatis-plus**](https://gitee.com/baomidou/mybatis-plus) 

- [**mall**](https://gitee.com/macrozheng/mall)

  > mall项目是一套电商系统，包括前台商城系统及后台管理系统，基于SpringBoot+MyBatis实现，采用Docker容器化部署

- [**hutool**](https://gitee.com/loolly/hutool)

- > Hutool是一个小而全的Java工具类库，使Java拥有函数式语言般的优雅，让Java语言也可以“甜甜的”
  >
  > [中文文档](https://www.hutool.cn/docs/#/) ：https://hutool.cn/

#### 数据库

##### 数据库文档

- **[screw](https://gitee.com/leshalv/screw)**

  > screw (螺丝钉) 英:[`skruː`] ~ 简洁好用的数据库表结构文档生成工具

### 面试

- [**JavaGuide**](https://github.com/Snailclimb/JavaGuide)

  

## 个人资源













